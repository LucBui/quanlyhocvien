/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qlhv.untility;

import com.qlhv.model.KhoaHoc;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LucBT
 */
public class ClassTableModel2 {
    public DefaultTableModel setTableKhoaHoc(List<KhoaHoc> listItem, String[] listColumn) {
        DefaultTableModel dtm = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnIndex == 7 ? Boolean.class : String.class;
            }
        };
        dtm.setColumnIdentifiers(listColumn);
        int columns = listColumn.length;
        Object[] obj = null;
        int rows = listItem.size();
        if(rows > 0){
           for (int i = 0; i < rows; i++) {
                KhoaHoc khoaHoc = listItem.get(i);  
                obj = new Object[columns];
                obj[0] = khoaHoc.getMa_khoa_hoc();
                obj[1] = (i + 1);
                obj[2] = khoaHoc.getTen_khoa_hoc();
                obj[3] = khoaHoc.getMo_ta();
                obj[4] = khoaHoc.getNgay_bat_dau();
                obj[5] = khoaHoc.getNgay_ket_thuc();
                obj[6] = khoaHoc.isTinh_trang();
                dtm.addRow(obj);
        }
        }
        return dtm;
    }
    
}
