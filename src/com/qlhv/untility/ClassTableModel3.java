/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qlhv.untility;

import com.qlhv.model.LopHoc;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LucBT
 */
public class ClassTableModel3 {
    public DefaultTableModel setTableLopHoc(List<LopHoc> listItem, String[] listColumn) {
        DefaultTableModel dtm = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnIndex == 7 ? Boolean.class : String.class;
            }
        };
        dtm.setColumnIdentifiers(listColumn);
        int columns = listColumn.length;
        Object[] obj = null;
        int rows = listItem.size();
        if(rows > 0){
           for (int i = 0; i < rows; i++) {
                LopHoc lopHoc = listItem.get(i);  
                obj = new Object[columns];
                obj[0] = lopHoc.getMa_lop_hoc();
                obj[1] = (i + 1);
                obj[2] = lopHoc.getMa_khoa_hoc();
                obj[3] = lopHoc.getMa_hoc_vien();
                obj[4] = lopHoc.getNgay_dang_ky();
                obj[5] = lopHoc.isTinh_trang();
                dtm.addRow(obj);
        }
        }
        return dtm;
    }
    
}
