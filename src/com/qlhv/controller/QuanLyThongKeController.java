
package com.qlhv.controller;

import com.qlhv.bean.KhoaHocBean;
import com.qlhv.bean.LopHocBean;
import com.qlhv.sevice.ThongKeService;
import com.qlhv.sevice.ThongKeServiceImpl;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.util.List;
import org.jfree.data.gantt.Task;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;


/**
 *
 * @author LucBT
 */
public class QuanLyThongKeController {

    private ThongKeService thongKeService = null;

    public QuanLyThongKeController() {
        this.thongKeService = new ThongKeServiceImpl();
    }

    public void setDataToChart1(JPanel jpnItem) {
        List<LopHocBean> listItem = thongKeService.getListByLopHoc();
        if (listItem != null) {
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            for (LopHocBean item : listItem) {
                dataset.addValue(item.getSoLuongHocVien(), "Học viên", item.getNgayDangKy());
            }

        JFreeChart barChart = ChartFactory.createBarChart(
                "THỐNG KÊ SỐ LƯỢNG HỌC VIÊN ĐĂNG KÝ".toUpperCase(),
                "Thời gian", "Số lượng",
                dataset);

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new Dimension(jpnItem.getWidth(), 300));

        jpnItem.removeAll();
        jpnItem.setLayout(new CardLayout());
        jpnItem.add(chartPanel);
        jpnItem.validate();
        jpnItem.repaint();
    }
    }
//    public void setDataToChart2(JPanel jpnItem) {
//        List<KhoaHocBean> listItem = thongKeService.getListByKhoaHoc();
//        if (listItem != null) {
//            TaskSeriesCollection ds = new TaskSeriesCollection();
//            TaskSeries taskSeries;
//            Task task;
//            for (KhoaHocBean item : listItem) {
//                taskSeries = new TaskSeries(item.getTenKhoaHoc());
//                task = new Task(item.getTenKhoaHoc(), item.getNgayBatDau(), item.getNgayKetThuc());
//                taskSeries.add(task);
//                ds.add(taskSeries);
//            }
//            JFreeChart chart = ChartFactory.createGanttChart(
//                "THỐNG KÊ TÌNH TRẠNG KHÓA HỌC",
//                "Khóa học", "Thời gian", ds);
//            ChartPanel chartPanel = new ChartPanel(chart);
//            chartPanel.setPreferredSize(new Dimension(jpnItem.getWidth(), 300));
//
//            jpnItem.removeAll();
//            jpnItem.setLayout(new CardLayout());
//            jpnItem.add(chartPanel);
//            jpnItem.validate();
//            jpnItem.repaint();
//    }
//    
//}
}
