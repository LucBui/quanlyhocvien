
package com.qlhv.controller;

import com.qlhv.model.HocVien;
import com.qlhv.sevice.HocVienService;
import com.qlhv.sevice.HocVienServiceImpl;
import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author LucBT
 */
public class HocVienController {
    
    private JButton btnSubmit;
    private JTextField jtfMaHocVien;
    private JTextField jtfHoTen;
    private JDateChooser jdcNgaySinh;
    private JTextField jtfSoDienThoai;
    private JRadioButton jtfNam;
    private JRadioButton jtfGioiTinhNu;
    private JTextArea jtaDiaChi;
    private JCheckBox jcbTinhTrang;
    private JLabel jlbMsg;
    
    private HocVien hocVien = null;
    
    private HocVienService hocVienService = null;
    
    
  
    public HocVienController(JButton btnSubmit, JTextField jtfMaHocVien, JTextField jtfHoTen,   
           JDateChooser jdcNgaySinh, JTextField jtfSoDienThoai, JRadioButton jtfGioiTinhNam, 
           JRadioButton jtfGioiTinhNu, JTextArea jtaDiaChi, JCheckBox jcbTinhTrang, JLabel jlbMsg) {
        this.btnSubmit = btnSubmit;
        this.jtfMaHocVien = jtfMaHocVien;
        this.jtfHoTen = jtfHoTen;
        this.jdcNgaySinh = jdcNgaySinh;
        this.jtfSoDienThoai = jtfSoDienThoai;
        this.jtfNam = jtfGioiTinhNam;
        this.jtfGioiTinhNu = jtfGioiTinhNu;
        this.jtaDiaChi = jtaDiaChi;
        this.jcbTinhTrang = jcbTinhTrang;
        this.jlbMsg = jlbMsg;
        
        this.hocVienService = new HocVienServiceImpl();
    }

  
    
    public void setView(HocVien hocVien) {
        this.hocVien = hocVien;
        jtfMaHocVien.setText("#" + hocVien.getMa_hoc_vien());
        jtfHoTen.setText(hocVien.getHo_ten());
        jdcNgaySinh.setDate(hocVien.getNgay_sinh());
        if (hocVien.isGioi_tinh()) {
            jtfNam.setSelected(true);
            jtfNam.setSelected(false);
        } else {
            jtfGioiTinhNu.setSelected(true);
            jtfGioiTinhNu.setSelected(false);
        }
        jtfSoDienThoai.setText(hocVien.getSo_dien_thoai());
        jtaDiaChi.setText(hocVien.getDia_chi());
        jcbTinhTrang.setSelected(hocVien.isTinh_trang());
        
    }
    public void setEvent(){ 
         btnSubmit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(jtfHoTen.getText().length() == 0 || jdcNgaySinh.getDate() == null){
                    jlbMsg.setText("Vui lòng nhập dữ liệu bắt buộc!");
            }else{
                      hocVien.setHo_ten(jtfHoTen.getText());
                      hocVien.setNgay_sinh((java.util.Date) jdcNgaySinh.getDate());
                      hocVien.setSo_dien_thoai(jtfSoDienThoai.getText());
                      hocVien.setDia_chi(jtaDiaChi.getText());
                      hocVien.setGioi_tinh(jtfNam.isSelected());
                      hocVien.setTinh_trang(jcbTinhTrang.isSelected());
                      int lastId = hocVienService.createOrUpdate(hocVien);
                      if(lastId > 0){
                      hocVien.setMa_hoc_vien(lastId);
                      jtfMaHocVien.setText("#" + lastId);
                      jlbMsg.setText("Cập nhật dữ liệu thông tin thành công!");}
                    
                }
            }
            
             @Override
            public void mouseEntered(MouseEvent e) {
                btnSubmit.setBackground(new Color(0, 200, 83));
            }
            
              @Override
            public void mouseExited(MouseEvent e) {
                btnSubmit.setBackground(new Color(100, 221, 23));
            }

             private Date covertDateToDateSql(java.sql.Date date) {
                 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
             }
         });
}
}
