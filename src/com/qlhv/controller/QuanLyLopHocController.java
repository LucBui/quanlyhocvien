/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qlhv.controller;

import com.qlhv.model.LopHoc;
import com.qlhv.sevice.LopHocService;
import com.qlhv.sevice.LopHocServiceImpl;
import com.qlhv.untility.ClassTableModel3;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author LucBT
 */
public class QuanLyLopHocController {
     private JPanel jpnView;
    private JTextField jtfSearch;
    
    private LopHocService lopHocService = null;
    
    private final String[] listColumn = {"Mã lớp học", "STT", "Mã khóa học", "Mã học viên", "Ngày đăng ký",
         "Trạng thái"};
    
    private TableRowSorter<TableModel> rowSorter = null;

    public QuanLyLopHocController(JPanel jpnView, JTextField jtfSearch) {
        this.jpnView = jpnView;
        this.jtfSearch = jtfSearch;
        
        this.lopHocService = new LopHocServiceImpl();
    }
    
        public void setDateToTable() {
        List<LopHoc> listItem = lopHocService.getList();
        
        DefaultTableModel model = new ClassTableModel3().setTableLopHoc(listItem, listColumn);
        
        JTable table = new JTable(model);

        rowSorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(rowSorter);

        jtfSearch.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = jtfSearch.getText();
                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = jtfSearch.getText();
                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }


        });
        
        table.getColumnModel().getColumn(1).setMaxWidth(80);
        table.getColumnModel().getColumn(1).setMinWidth(80);
        table.getColumnModel().getColumn(1).setPreferredWidth(80);
        
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && table.getSelectedRow() != -1) {
                    DefaultTableModel model = (DefaultTableModel) table.getModel();
                    int selectedRowIndex = table.getSelectedRow();
                    selectedRowIndex = table.convertRowIndexToModel(selectedRowIndex);
                    System.out.println(selectedRowIndex);
                    
                    LopHoc lopHoc = new LopHoc();
                    lopHoc.setMa_lop_hoc((int) model.getValueAt(selectedRowIndex, 0));
                    lopHoc.setMa_khoa_hoc((int) model.getValueAt(selectedRowIndex, 1));
                    lopHoc.setMa_hoc_vien((int) model.getValueAt(selectedRowIndex, 2));
                    lopHoc.setNgay_dang_ky((Date)model.getValueAt(selectedRowIndex, 4));
                    lopHoc.setTinh_trang((boolean) model.getValueAt(selectedRowIndex, 5));
                    
                   
                }
            }
            
        });

        table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 14));
        table.getTableHeader().setPreferredSize(new Dimension(100, 50));
        table.setRowHeight(50);
        table.validate();
        table.repaint();
        
        JScrollPane scroll = new JScrollPane();
        scroll.getViewport().add(table);
        scroll.setPreferredSize(new Dimension(1350, 400));
        jpnView.removeAll();
        jpnView.setLayout(new BorderLayout());
        jpnView.add(scroll);
        jpnView.validate();
        jpnView.repaint();
    }
    
}
