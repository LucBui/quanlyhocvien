
package com.qlhv.main;

import com.qlhv.view.DangNhapJDialog;
import com.qlhv.view.MainJFrame;

/**
 *
 * @author LucBT
 */
public class Main {
    public static void main(String[] args) {
//        new MainJFrame().setVisible(true);
        DangNhapJDialog dialog = new DangNhapJDialog(null, true);
        dialog.setTitle("Đăng Nhập Hệ Thống");
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }
    
}
