
package com.qlhv.sevice;

import com.qlhv.bean.KhoaHocBean;
import com.qlhv.bean.LopHocBean;
import java.util.List;

/**
 *
 * @author LucBT
 */
public interface ThongKeService {
    public List<LopHocBean> getListByLopHoc();
    
     public List<KhoaHocBean> getListByKhoaHoc();
    
}
