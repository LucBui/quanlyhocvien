/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qlhv.sevice;

import com.qlhv.dao.KhoaHocDAO;
import com.qlhv.dao.KhoaHocDAOImpl;
import com.qlhv.model.KhoaHoc;
import java.util.List;

/**
 *
 * @author LucBT
 */
public class KhoaHocServiceImpl implements KhoaHocService{

    private KhoaHocDAO khoaHocDAO = null;

    public KhoaHocServiceImpl() {
        khoaHocDAO = new KhoaHocDAOImpl();
    }

    @Override
    public List<KhoaHoc> getList() {
        return khoaHocDAO.getList();
    }

    @Override
    public int createOrUpdate(KhoaHoc khoaHoc) {
       return khoaHocDAO.createOrUpdate(khoaHoc); 
    }

 
}
