
package com.qlhv.sevice;

import com.qlhv.dao.TaiKhoanDAO;
import com.qlhv.dao.TaiKhoanDAOImpl;
import com.qlhv.model.TaiKhoan;

/**
 *
 * @author LucBT
 */
public class TaiKhoanServiceImpl implements TaiKhoanService {

    private TaiKhoanDAO taiKhoanDAO = null;

    public TaiKhoanServiceImpl() {
        taiKhoanDAO = (TaiKhoanDAO) new TaiKhoanDAOImpl();
    }

    @Override
    public TaiKhoan login(String tdn, String mk) {
        return taiKhoanDAO.login(tdn, mk);
    }
    
}
