
package com.qlhv.sevice;

import com.qlhv.bean.KhoaHocBean;
import com.qlhv.model.HocVien;
import java.util.List;

public interface HocVienService {
    
    public List<HocVien> getList();
    
     public int createOrUpdate(HocVien hocVien);
    
    
}