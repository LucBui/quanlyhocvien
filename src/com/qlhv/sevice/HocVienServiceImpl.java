
package com.qlhv.sevice;

import com.qlhv.dao.HocVienDAO;
import com.qlhv.dao.HocVienDAOImpl;
import com.qlhv.model.HocVien;
import java.util.List;

public class HocVienServiceImpl implements HocVienService {

    private HocVienDAO hocVienDAO = null;

    public HocVienServiceImpl() {
        hocVienDAO = new HocVienDAOImpl();
    }

    @Override
    public List<HocVien> getList() {
        return hocVienDAO.getList();
    }

    @Override
    public int createOrUpdate(HocVien hocVien) {
       return hocVienDAO.createOrUpdate(hocVien); 
    }

}
