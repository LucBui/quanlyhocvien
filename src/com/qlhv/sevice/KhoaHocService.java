
package com.qlhv.sevice;

import com.qlhv.model.KhoaHoc;
import java.util.List;

/**
 *
 * @author LucBT
 */
public interface KhoaHocService {
    public List<KhoaHoc> getList();
    
     public int createOrUpdate(KhoaHoc khoaHoc);
    
}
