
package com.qlhv.sevice;

import com.qlhv.bean.KhoaHocBean;
import com.qlhv.bean.LopHocBean;
import com.qlhv.dao.ThongKeDAO;
import com.qlhv.dao.ThongKeDAOImpl;
import java.util.List;

/**
 *
 * @author LucBT
 */
public class ThongKeServiceImpl implements ThongKeService{
    private ThongKeDAO thongKeDAO = null;

    public ThongKeServiceImpl() {
        this.thongKeDAO = new ThongKeDAOImpl();
    }


    @Override
    public List<LopHocBean> getListByLopHoc() {
        return thongKeDAO.getListByLopHoc();
    }

    @Override
    public List<KhoaHocBean> getListByKhoaHoc() {
        return thongKeDAO.getListByKhoaHoc();

    }
    
}
