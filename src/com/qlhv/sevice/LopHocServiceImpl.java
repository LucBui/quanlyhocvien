/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qlhv.sevice;

import com.qlhv.dao.LopHocDAO;
import com.qlhv.dao.LopHocDAOImpl;
import com.qlhv.model.LopHoc;
import java.util.List;

/**
 *
 * @author LucBT
 */
public class LopHocServiceImpl implements LopHocService{
    
     private LopHocDAO lopHocDAO = null;

    public LopHocServiceImpl() {
        lopHocDAO = new LopHocDAOImpl();
    }

    @Override
    public List<LopHoc> getList() {
        return lopHocDAO.getList();
    }

    @Override
    public int createOrUpdate(LopHoc lopHoc) {
       return lopHocDAO.createOrUpdate(lopHoc); 
    }
    
    
}
