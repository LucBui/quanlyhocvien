
package com.qlhv.sevice;

import com.qlhv.model.TaiKhoan;

/**
 *
 * @author LucBT
 */
public interface TaiKhoanService {
    
    public TaiKhoan login(String tenDangNhap, String matKhau);
    
}
