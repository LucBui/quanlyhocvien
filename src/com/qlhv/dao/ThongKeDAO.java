
package com.qlhv.dao;

import com.qlhv.bean.KhoaHocBean;
import com.qlhv.bean.LopHocBean;
import java.util.List;

/**
 *
 * @author LucBT
 */
public interface ThongKeDAO {
    
    public List<LopHocBean> getListByLopHoc();
    public List<KhoaHocBean> getListByKhoaHoc();
    
}
