
package com.qlhv.dao;

import com.qlhv.model.TaiKhoan;

/**
 *
 * @author LucBT
 */
public interface TaiKhoanDAO {
    
    public TaiKhoan login(String tdn, String mk);
    
    
}
