/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qlhv.dao;

import com.qlhv.model.LopHoc;
import java.util.List;

/**
 *
 * @author LucBT
 */
public interface LopHocDAO {
    public List<LopHoc> getList();

    public int createOrUpdate(LopHoc lopHoc);
    
}
