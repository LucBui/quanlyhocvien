    
package com.qlhv.dao;

import com.qlhv.bean.KhoaHocBean;
import com.qlhv.bean.LopHocBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LucBT
 */
public class ThongKeDAOImpl implements ThongKeDAO{

    @Override
    public List<LopHocBean> getListByLopHoc() {
     Connection cons = DBConnect.getConnection();
        String sql = "select ngay_dang_ky, count(*) as so_luong from lop_hoc group by ngay_dang_ky";
        List<LopHocBean> list = new ArrayList<>();
        try {
            PreparedStatement ps = (PreparedStatement) cons.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                LopHocBean lopHocBean = new LopHocBean();
                lopHocBean.setNgayDangKy(rs.getString("ngay_dang_ky"));
                lopHocBean.setSoLuongHocVien(rs.getInt("so_luong"));
                list.add(lopHocBean);
            }
            ps.close();
            cons.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
}

    @Override
    public List<KhoaHocBean> getListByKhoaHoc() {
         Connection cons = DBConnect.getConnection();
        String sql = "select ten_khoa_hoc, ngay_bat_dau, ngay_ket_thuc from khoa_hoc where tinh_trang = true";
        List<KhoaHocBean> list = new ArrayList<>();
        try {
            PreparedStatement ps = (PreparedStatement) cons.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                KhoaHocBean khoaHocBean = new KhoaHocBean();
                khoaHocBean.setTenKhoaHoc(rs.getString("ten_khoa_hoc"));
                khoaHocBean.setNgayBatDau(rs.getDate("ngay_bat_dau"));
                khoaHocBean.setNgayKetThuc(rs.getDate("ngay_ket_thuc"));

                list.add(khoaHocBean);
            }
            ps.close();
            cons.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
}
}
